=================
*INI* data reader
=================

*galactic-io-data-ini* is an
`INI <https://en.wikipedia.org/wiki/INI_file>`_
data reader plugin for **GALACTIC**.

The file extension is ``.ini``. The individuals are represented in named
sections by `key = "value"` pairs. For example:

.. code-block:: ini
    :class: admonition

    [#1]
    name = "Galois"
    firstname = "Évariste"
    [#2]
    name = "Wille"
    firstname = "Rudolf"

This reader uses the ``configparser.ConfigParser`` class of the
`python core library <https://docs.python.org/3/library/configparser.html#configparser.ConfigParser>`_.

