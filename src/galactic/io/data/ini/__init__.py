"""
FIMI Data reader.
"""

from ._main import INIDataReader, register

__all__ = ("INIDataReader", "register")
