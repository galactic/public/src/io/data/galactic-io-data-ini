"""
``INI`` Data reader.
"""

from __future__ import annotations

import configparser
from typing import TYPE_CHECKING, TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory

if TYPE_CHECKING:
    from collections.abc import Iterator, Mapping


# pylint: disable=too-few-public-methods
class INIDataReader:
    """
    ``INI`` Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.ini import INIDataReader
    >>> reader = INIDataReader()
    >>> import io
    >>> data = '''[#1]
    ... name=Galois
    ... firstname=Évariste
    ... [#2]
    ... name=Wille
    ... firstname=Rudolf
    ... '''
    >>> individuals = reader.read(io.StringIO(data))
    >>> pprint(dict(individuals.items()))
    {'#1': {'firstname': 'Évariste', 'name': 'Galois'},
     '#2': {'firstname': 'Rudolf', 'name': 'Wille'}}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a ``ini`` data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        config = configparser.ConfigParser()
        config.read_file(data_file)
        identifiers = config.sections()
        individuals: dict[str, object] = {}
        for identifier in identifiers:
            individuals[identifier] = dict(config[identifier])
        return individuals

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".ini"])


def register() -> None:
    """
    Register an instance of a ``INI`` data reader.
    """
    PopulationFactory.register_reader(INIDataReader())
