"""Population test module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.ini import INIDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, INIDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
