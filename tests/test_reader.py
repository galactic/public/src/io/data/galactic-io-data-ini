"""Reader test module."""

from pathlib import Path
from unittest import TestCase

from galactic.io.data.core import PopulationFactory


class IniDataReaderTest(TestCase):
    def test_get_reader(self):
        pathname = Path(__file__).parent / "test.ini"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "#1": {"name": "Galois", "firstname": "Évariste"},
                "#2": {"name": "Wille", "firstname": "Rudolf"},
            },
        )
